package com.edu.serg.util;

import java.io.File;
import java.util.*;
import java.util.stream.Stream;

public class LocaleManager {

    private static final String BUNDLE_NAME = "MessageBundle";
    private static final String BUNDLE_PATH = String.join(File.separator, "i18n", BUNDLE_NAME);
    private static final Locale[] AVAILABLE_LOCALES = {Locale.UK, Locale.GERMANY, Locale.FRANCE};

    private Map<Locale, ResourceBundle> bundles;

    public LocaleManager() {
        initBundles();
    }

    public String getMessage(String key, Locale locale) {
        if (Stream.of(AVAILABLE_LOCALES).noneMatch(l -> l.equals(locale))) {
            throw new IllegalArgumentException("Unknown locale " + locale);
        }
        return bundles.get(locale).getString(key);
    }

    private void initBundles() {
        bundles = new HashMap<>();
        Stream.of(AVAILABLE_LOCALES).forEach(locale -> bundles.put(locale, ResourceBundle.getBundle(BUNDLE_PATH, locale)));
    }
}
