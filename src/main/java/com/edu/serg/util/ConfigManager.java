package com.edu.serg.util;

import java.io.IOException;
import java.util.Properties;

public class ConfigManager {

    private static final String CONFIG_PATH = "/config.properties";

    private Properties properties;

    public ConfigManager() {
        loadConfigs();
    }

    public String getConfig(String key) {
        return properties.getProperty(key);
    }

    public void reloadConfigs() {
        loadConfigs();
    }

    private void loadConfigs() {
        try {
            properties = new Properties();
            properties.load(getClass().getResourceAsStream(CONFIG_PATH));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
