package com.edu.serg.models;

import java.util.Locale;

public class LocaleContext {

    private static Locale currentLocale;

    public static Locale getCurrentLocale() {
        return currentLocale;
    }

    public static void setCurrentLocale(Locale currentLocale) {
        LocaleContext.currentLocale = currentLocale;
    }
}
