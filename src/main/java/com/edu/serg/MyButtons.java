package com.edu.serg;

import com.edu.serg.util.ConfigManager;
import com.edu.serg.util.LocaleManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;
import java.util.Properties;

public class MyButtons{

    private static final LocaleManager LOCALE_MANAGER = new LocaleManager();
    private static final ConfigManager CONFIG_MANAGER = new ConfigManager();

    String languageSave="en", countrySave="GB";

    MyButtons(){
        JFrame f= new JFrame("Home Work 1");
        JButton b1,b2,b3,save,load;
        JTextField txt1, txt2, txt3;
        Properties prop = new Properties();

        b1=new JButton(new ImageIcon("src\\main\\resources\\images\\en.png"));
        b1.setBounds(220,50,60,40);
        f.add(b1);

        b2=new JButton(new ImageIcon("src\\main\\resources\\images\\fr.png"));
        b2.setBounds(300,50,60,40);
        f.add(b2);

        b3=new JButton(new ImageIcon("src\\main\\resources\\images\\de.png"));
        b3.setBounds(380,50,60,40);
        f.add(b3);

        save=new JButton("Сохранить");
        save.setBounds(180,300,120,40);
        f.add(save);
        load=new JButton("Загрузить");
        load.setBounds(330,300,120,40);
        f.add(load);
        JLabel pudge = new JLabel(new ImageIcon("src\\main\\resources\\images\\pudge.png"));
        pudge.setBounds(10,80, 200,200);
        f.add(pudge);

        txt1= new JTextField();
        txt1.setBounds(220,120,220,30);
        f.add(txt1);

        txt2= new JTextField();
        txt2.setBounds(220,165,220,30);
        f.add(txt2);

        txt3= new JTextField();
        txt3.setBounds(220,210,220,30);
        f.add(txt3);

        reloadButton(b1, save, load, txt1, txt2, txt3, "en", "GB");
        reloadButton(b2, save, load, txt1, txt2, txt3, "fr", "FR");
        reloadButton(b3, save, load, txt1, txt2, txt3, "de", "DE");

        load.addActionListener(e -> {
            String country = CONFIG_MANAGER.getConfig("country");
            String language = CONFIG_MANAGER.getConfig("language");
            Locale locale = new Locale(language, country);
            txt1.setText(LOCALE_MANAGER.getMessage("pudge.phrase.greetings", locale));
            txt2.setText(LOCALE_MANAGER.getMessage("pudge.phrase.farewell", locale));
            txt3.setText(LOCALE_MANAGER.getMessage("pudge.phrase.inquiry", locale));
            b2.setText(LOCALE_MANAGER.getMessage("save.button.label", locale));
            b3.setText(LOCALE_MANAGER.getMessage("load.button.label", locale));
        });

        save.addActionListener(actionEvent -> {
            prop.setProperty("language", languageSave);
            prop.setProperty("country", countrySave);
            FileOutputStream out;
            try {
                URL resourceUrl = getClass().getResource("/config.properties");
                File file = new File(resourceUrl.toURI());
                out = new FileOutputStream(file);
                prop.store(out,null);
                out.close();
            } catch (IOException | URISyntaxException e) {
                e.printStackTrace();
            } finally {
                CONFIG_MANAGER.reloadConfigs();
            }
        });

        f.setSize(500,400);
        f.setLayout(null);
        f.setVisible(true);
        f.setLocationRelativeTo(null);
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }

    public void reloadButton(JButton button1, JButton button2, JButton button3,
                             JTextField text1, JTextField text2, JTextField text3, String l, String c) {
        button1.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e)
            {
                languageSave = l;
                countrySave = c;
                Locale locale = new Locale(l, c);
                text1.setText(LOCALE_MANAGER.getMessage("pudge.phrase.greetings", locale));
                text2.setText(LOCALE_MANAGER.getMessage("pudge.phrase.farewell", locale));
                text3.setText(LOCALE_MANAGER.getMessage("pudge.phrase.inquiry", locale));
                button2.setText(LOCALE_MANAGER.getMessage("save.button.label", locale));
                button3.setText(LOCALE_MANAGER.getMessage("load.button.label", locale));
            }
        });
    }
}    