package com.edu.serg.presenter;

import javax.swing.*;
import java.util.Locale;

public class SwitchLocaleButton extends JButton {

    private Locale locale;

    public SwitchLocaleButton(Icon icon, Locale locale) {
        super(icon);
        this.locale = locale;
    }

    public Locale getLocale() {
        return locale;
    }
}
